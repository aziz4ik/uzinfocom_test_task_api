# Use an official Python runtime as the base image
FROM python:3.9

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install system dependencies
RUN apt-get update \
    && apt-get install -y postgresql-client \
                           libgdal-dev \
                           gdal-bin \
    && rm -rf /var/lib/apt/lists/*

# Set the GDAL library path environment variable
ENV GDAL_LIBRARY_PATH=/usr/lib/libgdal.so

# Set the working directory in the container
WORKDIR /code

# Install dependencies
COPY requirements.txt /code/
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install GDAL==3.2.2.1


# Copy the application code to the container
COPY . /code/

# Expose the port that the Django app will run on
EXPOSE 8000

# Run any additional setup commands (if required)
# For example, if you need to run migrations
# RUN python manage.py migrate

# Run the Django development server
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
