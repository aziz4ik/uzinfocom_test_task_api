from django.contrib.auth.models import User
from django.db import models
from django.db.models import PROTECT


class BaseModel(models.Model):
    """
    An abstract base model class that provides common fields for created and modified timestamps,
    as well as tracking the user responsible for creating and modifying records.

    Attributes:
        created_at (DateTimeField): The timestamp when the record was created. Automatically set on creation.
        updated_at (DateTimeField): The timestamp when the record was last updated. Automatically updated on save.
        created_by (ForeignKey): Reference to the User who created the record. Can be null and blank.
        modified_by (ForeignKey): Reference to the User who last modified the record. Can be null and blank.
    """

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # created_by = models.ForeignKey(User,
    #                                on_delete=PROTECT,
    #                                related_name='%(class)s_createdby',
    #                                null=True,
    #                                blank=True)
    # modified_by = models.ForeignKey(User,
    #                                 on_delete=PROTECT,
    #                                 related_name='%(class)s_modifiedby',
    #                                 null=True,
    #                                 blank=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """
        Save method that sets the 'created_by' and 'modified_by' fields based on the request user.

        If the instance is being created (i.e., it does not have a primary key yet), 'created_by' is set
        to the current user. In all cases, 'modified_by' is set to the current user.

        Args:
            *args: Variable length argument list.
            **kwargs: Arbitrary keyword arguments. Expected to contain 'request' to set user fields.

        Keyword Args:
            request (HttpRequest): The HTTP request object, used to access the current user.
        """
        request = kwargs.pop('request', None)
        if request:
            if not self.pk:  # If the object is new
                self.created_by = request.user
            self.modified_by = request.user
        super().save(*args, **kwargs)
