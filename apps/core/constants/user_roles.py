SUPER_ADMIN = 'super_admin'
CUSTOMER = 'customer'
STADIUM_OWNER = 'stadium_owner'

USER_ROLES = (
    (SUPER_ADMIN, 'super_admin'),
    (CUSTOMER, 'customer'),
    (STADIUM_OWNER, 'stadium_owner'),
)
