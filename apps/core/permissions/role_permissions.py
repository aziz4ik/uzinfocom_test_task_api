from rest_framework import permissions

class IsStadiumOwnerOrSuperAdminOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        # Allow anyone to see stadium details and lists
        if view.action in ['list', 'retrieve', 'closest_stadiums']:
            return True

        if request.user.is_authenticated:
            # Allow authenticated users with role 'stadium_owner' to create stadiums
            if view.action == 'create' and request.user.role == 'stadium_owner':
                return True

            # Allow superuser to perform any action
            if request.user.is_superuser:
                return True

            # Allow authenticated users with role 'customer' or 'superuser' to reserve stadiums
            if view.action == 'reserve' and (request.user.role == 'customer' or request.user.is_superuser):
                return True

        return False

    def has_object_permission(self, request, view, obj):
        # Allow stadium owner to modify their own stadium and superuser to modify any stadium
        if request.method in ['PUT', 'PATCH', 'DELETE']:
            if request.user.is_superuser or (obj.user == request.user and request.user.role == 'stadium_owner'):
                return True

        # Allow read-only access for any request method
        return request.method in permissions.SAFE_METHODS


class IsStadiumOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated and request.user.role == 'stadium_owner'
