from django.contrib.auth import authenticate
from rest_framework import viewsets, status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken


from apps.user.models import User
from apps.user.serializers.user import UserSerializer, StadiumOwnerSerializer


class CustomerCreateViewSet(viewsets.ModelViewSet):
    model = User
    queryset = User.objects.none()
    serializer_class = UserSerializer
    http_method_names = ['post']

    def create(self, request, *args, **kwargs):
        # Check if stadium_id is sent in request data
        if 'stadium_id' in request.data:
            return Response({"error": "This endpoint only accepts user fields and does not accept stadium_id"},
                            status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)




class StadiumOwnerCreateViewSet(viewsets.ModelViewSet):
    queryset = User.objects.none()
    serializer_class = StadiumOwnerSerializer
    permission_classes = [permissions.AllowAny]  # Allow anyone to create a stadium owner
    http_method_names = ['post']



class UserLoginView(APIView):
    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')

        # Check if both username and password are provided
        if not username or not password:
            return Response({'error': 'Both username and password are required'}, status=status.HTTP_400_BAD_REQUEST)

        # Authenticate user
        user = authenticate(username=username, password=password)

        if user is not None:
            # User authentication successful
            refresh = RefreshToken.for_user(user)
            return Response({
                'access': str(refresh.access_token),
                'refresh': str(refresh),
            }, status=status.HTTP_200_OK)
        else:
            # Invalid credentials
            return Response({'error': 'Invalid username or password'}, status=status.HTTP_401_UNAUTHORIZED)
