from rest_framework import serializers

from apps.user.models import User
from apps.stadium.models import Stadium


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['username', 'password', 'gender', 'phone_number', 'first_name', 'last_name', 'email']

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
            gender=validated_data.get('gender'),
            phone_number=validated_data.get('phone_number'),
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
            email=validated_data.get('email'),
            role='customer'  # Create user with customer role
        )
        return user


class StadiumOwnerSerializer(serializers.ModelSerializer):
    stadiums = serializers.PrimaryKeyRelatedField(queryset=Stadium.objects.all(), many=True, required=False)

    class Meta:
        model = User
        fields = ['username', 'password', 'gender', 'phone_number', 'first_name', 'last_name', 'email', 'stadiums', 'role']
        extra_kwargs = {
            'password': {'write_only': True},  # Password should not be included in response
            'role': {'read_only': True}  # Role should be fixed as 'stadium_owner'
        }

    def create(self, validated_data):
        stadiums_data = validated_data.pop('stadiums', [])
        user = User.objects.create_user(role='stadium_owner', **validated_data)

        for stadium_data in stadiums_data:
            user.stadiums.add(stadium_data)

        return user
