from django.urls import path, include
from rest_framework import routers

from apps.user import views

router = routers.DefaultRouter()
router.register('create_customer', views.CustomerCreateViewSet, 'create_customer'),
router.register('create_stadium_owner', views.StadiumOwnerCreateViewSet, 'create_stadium_owner'),

urlpatterns = [
    path('', include(router.urls)),
    path('login/', views.UserLoginView.as_view(), name='login'),
]
