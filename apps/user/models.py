from django.contrib.auth.models import AbstractUser
from django.db import models

from apps.core.constants.genders import GENDER_TYPE
from apps.stadium.models import Stadium
from apps.core.constants.user_roles import USER_ROLES


class User(AbstractUser):
    stadiums = models.ManyToManyField(Stadium, related_name='user_stadiums')
    phone_number = models.PositiveIntegerField(unique=True, null=True, blank=True)
    gender = models.CharField(max_length=55, choices=GENDER_TYPE)
    role = models.CharField(max_length=55, choices=USER_ROLES)

    def __str__(self):
        return self.username
