from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.stadium import views

router = DefaultRouter()

router.register('stadiums', views.StadiumModelViewSet, 'stadium')

urlpatterns = [
    path('', include(router.urls)),
]
