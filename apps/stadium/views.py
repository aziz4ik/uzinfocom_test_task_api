from datetime import datetime

from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import Distance

from rest_framework.decorators import action
from rest_framework import filters, permissions
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import ValidationError
from datetime import datetime
from django.db.models import Q
from rest_framework.viewsets import ModelViewSet

from apps.stadium.models import Stadium
from apps.stadium.serializers.stadiums import StadiumSerializer
from apps.core.permissions.role_permissions import IsStadiumOwnerOrSuperAdminOrReadOnly
from apps.reservations.models import StadiumReservation
from apps.reservations.serializers.stadium_reservations import StadiumReservationSerializer


class StadiumModelViewSet(ModelViewSet):
    model = Stadium
    queryset = Stadium.objects.all()
    serializer_class = StadiumSerializer
    permission_classes = [IsStadiumOwnerOrSuperAdminOrReadOnly]
    filter_backends = [filters.SearchFilter]
    search_fields = ('name',)

    def get_queryset(self):
        queryset = super().get_queryset()

        from_date_str = self.request.query_params.get('from_date_time')
        to_date_str = self.request.query_params.get('to_date_time')

        if from_date_str and to_date_str:
            try:
                from_date = datetime.fromisoformat(from_date_str)
                to_date = datetime.fromisoformat(to_date_str)
            except ValueError:
                raise ValidationError("Invalid date format. Use 'YYYY-MM-DDTHH:MM:SS'.")

            # Filter out stadiums that have reservations overlapping the given time range
            overlapping_reservations = StadiumReservation.objects.filter(
                Q(start_date_time__lt=to_date) & Q(end_date_time__gt=from_date)
            ).values_list('stadium_id', flat=True)

            queryset = queryset.exclude(id__in=overlapping_reservations)

        return queryset

    @action(detail=True, methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def reserve(self, request, pk=None):
        stadium = self.get_object()

        # Extract reservation details from request data
        start_date_time_str = request.data.get('start_date_time')
        end_date_time_str = request.data.get('end_date_time')

        if not start_date_time_str or not end_date_time_str:
            return Response({'error': 'Start date time and end date time are required'},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            start_date_time = datetime.fromisoformat(start_date_time_str)
            end_date_time = datetime.fromisoformat(end_date_time_str)
        except ValueError:
            return Response({'error': 'Invalid date format. Use "YYYY-MM-DDTHH:MM:SS"'},
                            status=status.HTTP_400_BAD_REQUEST)

        # Check for overlapping reservations
        if StadiumReservation.objects.filter(
                stadium=stadium,
                start_date_time__lt=end_date_time,
                end_date_time__gt=start_date_time
        ).exists():
            return Response({'error': 'This time slot is already reserved'}, status=status.HTTP_400_BAD_REQUEST)

        # Create the reservation
        reservation = StadiumReservation.objects.create(
            stadium=stadium,
            reserved_by=request.user,
            start_date_time=start_date_time,
            end_date_time=end_date_time
        )

        serializer = StadiumReservationSerializer(reservation)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['get'], permission_classes=[permissions.AllowAny])
    def closest_stadiums(self, request):
        lat = request.query_params.get('lat')
        lon = request.query_params.get('lon')

        if not lat or not lon:
            return Response({'error': 'Latitude and longitude are required'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            latitude = float(lat)
            longitude = float(lon)
        except ValueError:
            return Response({'error': 'Invalid latitude or longitude format'}, status=status.HTTP_400_BAD_REQUEST)

        user_location = Point(longitude, latitude, srid=4326)

        stadiums = Stadium.objects.annotate(
            distance=Distance('location', user_location)
        ).order_by('distance')

        if stadiums.exists():
            serializer = StadiumSerializer(stadiums, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'No stadiums found'}, status=status.HTTP_404_NOT_FOUND)
