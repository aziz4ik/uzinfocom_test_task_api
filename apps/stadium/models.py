from django.db import models
from django.contrib.gis.db import models as gis_models


from apps.core.models.base_model import BaseModel


class Stadium(BaseModel):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    contact = models.PositiveIntegerField()
    reservation_price_per_hour = models.PositiveIntegerField()
    location = gis_models.PointField(geography=True, blank=True, null=True)

    def __str__(self):
        return self.name


class StadiumImage(BaseModel):
    stadium = models.ForeignKey(Stadium, on_delete=models.CASCADE, related_name='stadium_images')
    image = models.ImageField(upload_to='stadium_images/')

    def __str__(self):
        return f'{self.stadium.id}'
