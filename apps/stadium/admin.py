from django.contrib import admin

from apps.stadium.models import Stadium, StadiumImage


class StadiumImageInline(admin.TabularInline):
    model = StadiumImage
    extra = 1  # Number of extra inline forms to display


@admin.register(Stadium)
class StadiumAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'address', 'contact', 'reservation_price_per_hour')
    inlines = [StadiumImageInline]  # Add the inline for StadiumImage