from rest_framework import serializers
from django.contrib.gis.geos import Point


from apps.stadium.models import Stadium, StadiumImage


class LocationField(serializers.Field):
    def to_representation(self, value):
        # Convert Point object to string representation
        return f"{value.y}, {value.x}"

    def to_internal_value(self, data):
        # Parse string representation to create a Point object
        try:
            latitude, longitude = map(float, data.split(","))
            return Point(longitude, latitude)
        except (ValueError, TypeError):
            raise serializers.ValidationError("Invalid coordinates format")


class StadiumImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = StadiumImage
        fields = ('image',)


class StadiumSerializer(serializers.ModelSerializer):
    location = LocationField()
    stadium_images = StadiumImageSerializer(many=True, required=False)  # Nested serializer for images

    class Meta:
        model = Stadium
        fields = ( 
                  'id',
                  'name',
                  'address',
                  'contact',
                  'reservation_price_per_hour',
                  'location',
                  'stadium_images'
                  )

    def create(self, validated_data):
        # Pop out stadium_images data as it will be processed separately
        stadium_images_data = validated_data.pop('stadium_images', [])

        # Create the stadium instance
        stadium = Stadium.objects.create(**validated_data)

        # Create stadium images and associate them with the stadium
        for image_data in stadium_images_data:
            StadiumImage.objects.create(stadium=stadium, **image_data)

        return stadium
