from rest_framework import viewsets

from apps.reservations.models import StadiumReservation
from apps.reservations.serializers.stadium_reservations import StadiumReservationSerializer, \
    StadiumOwnerReservationSerializer
from apps.core.permissions.role_permissions import IsStadiumOwner


class AllReservationsModelViewSet(viewsets.ModelViewSet):
    model = StadiumReservation
    serializer_class = StadiumReservationSerializer
    queryset = StadiumReservation.objects.all()
    # permission_classes =


class StadiumOwnerReservations(viewsets.ModelViewSet):
    model = StadiumReservation
    serializer_class = StadiumOwnerReservationSerializer
    queryset = StadiumReservation.objects.all()
    http_method_names = ['get', 'delete']
    permission_classes = [IsStadiumOwner]

    def get_queryset(self):
        # Retrieve stadiums owned by the requesting user
        stadiums_owned = self.request.user.stadiums.all()
        # Filter reservations by stadiums owned by the requesting user
        return StadiumReservation.objects.filter(stadium__in=stadiums_owned)
