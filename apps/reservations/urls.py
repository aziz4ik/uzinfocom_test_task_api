from django.urls import path, include
from rest_framework import routers

from apps.reservations import views

router = routers.DefaultRouter()
router.register('reservations', views.AllReservationsModelViewSet, 'reservations')
router.register('stadium_owner_reservations', views.StadiumOwnerReservations, 'stadium_owner_reservations')

urlpatterns = [
    path('', include(router.urls)),
]
