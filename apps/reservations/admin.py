from django.contrib import admin

from apps.reservations.models import StadiumReservation


@admin.register(StadiumReservation)
class StadiumReservationAdmin(admin.ModelAdmin):
    list_display = ['id', 'stadium', 'start_date_time', 'end_date_time', 'reserved_by']
    list_filter = ['stadium', 'start_date_time', 'end_date_time', 'reserved_by']
