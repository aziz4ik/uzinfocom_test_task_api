from rest_framework import serializers

from apps.reservations.models import StadiumReservation


class StadiumReservationSerializer(serializers.ModelSerializer):
    stadium_name = serializers.CharField(source='stadium.name', read_only=True)
    reserved_by_username = serializers.CharField(source='reserved_by.username', read_only=True)

    class Meta:
        model = StadiumReservation
        fields = ('id', 'stadium_name', 'start_date_time', 'end_date_time', 'reserved_by_username')


class StadiumOwnerReservationSerializer(serializers.ModelSerializer):
    stadium_name = serializers.CharField(source='stadium.name', read_only=True)
    reserved_by_username = serializers.CharField(source='reserved_by.username', read_only=True)

    class Meta:
        model = StadiumReservation
        fields = ('id', 'stadium_name', 'start_date_time', 'end_date_time', 'reserved_by_username')
