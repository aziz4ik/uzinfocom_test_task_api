from django.db import models

from apps.core.models.base_model import BaseModel
from apps.user.models import User
from apps.stadium.models import Stadium


class StadiumReservation(BaseModel):
    stadium = models.ForeignKey(Stadium, on_delete=models.CASCADE)
    reserved_by = models.ForeignKey(User, on_delete=models.CASCADE)
    start_date_time = models.DateTimeField()
    end_date_time = models.DateTimeField()

    def __str__(self):
        return f'{self.stadium} - {self.start_date_time} - {self.end_date_time}'

